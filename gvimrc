set guifont=Monospace\ 15
set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=b
imap :!setxkbmap us:!setxkbmap us,ru
nmap :!setxkbmap us:!setxkbmap us,ru
