﻿lua << EOF
--local dap = require('dap')
local hasdap,dap = pcall(require,"dap")
if hasdap
    then
    dap.adapters.ruby = {
        type = 'executable';
        command = 'bundle';
        args = {'exec', 'readapt', 'stdio'};
        }

    dap.configurations.ruby = {
        {
                type = 'ruby';
                request = 'launch';
                name = 'Rails';
                program = 'bundle';
                programArgs = {'exec', 'rails', 's'};
                useBundler = true;
        },
    }
end
EOF

nnoremap <silent> <F5> :lua require'dap'.continue()<CR>
nnoremap <silent> <F10> :lua require'dap'.step_over()<CR>
nnoremap <silent> <F11> :lua require'dap'.step_into()<CR>
nnoremap <silent> <F12> :lua require'dap'.step_out()<CR>
nnoremap <silent> <leader>b :lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <leader>B :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nnoremap <silent> <leader>lp :lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nnoremap <silent> <leader>dr :lua require'dap'.repl.open()<CR>
nnoremap <silent> <leader>dl :lua require'dap'.run_last()<CR>


let g:org_aggressive_conceal = 1
autocmd FileType org set conceallevel=2

lua << EOF
-- Load custom treesitter grammar for org filetype
require('orgmode').setup_ts_grammar()
require'nvim-treesitter.configs'.setup {
    -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
    highlight = {
    enable = true,
--    disable = {'org'}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
    additional_vim_regex_highlighting = {'org'}, -- Required since TS highlighter doesn't support all syntax features (conceal)
    },
  ensure_installed = {'org'}, -- Or run :TSUpdate org
  }

require('orgmode').setup({
    org_agenda_files = {'~/Dropbox/me/org/{today,inbox,week,tasks}.org'},
    --org_agenda_files = {'~/Dropbox/me/org/tasks.org'},
    org_default_notes_file = '~/Dropbox/me/org/today.org',
    org_agenda_start_on_weekday = false,
    org_highlight_latex_and_related = 'entities',
    --org_agenda_skip_scheduled_if_done = true,
    --org_agenda_skip_deadline_if_done = true,
    --org_indent_mode = noindent
})

require('gitsigns').setup()

require('tabnine').setup()
--require('tabnine').setup({
----  disable_auto_comment=true,
--  accept_keymap="<Tab>",
--  dismiss_keymap = "<C-]>",
----  debounce_ms = 800,
---- suggestion_color = {gui = "#808080", cterm = 244},
--  exclude_filetypes = {"org"},
---- log_file_path = nil, -- absolute path to Tabnine log file
--})
EOF
