" Выполнить задачу
if has("nvim")
    nmap <localleader>dt cit<ESC>darG<ESC>pG<C-o><C-o>zx:e<CR>
else
    nmap <localleader>dt <Plug>OrgTodoToggleNonInteractive<ESC>darGp<C-o>:e<CR>
endif

" Все выполненные перекинуть в соотв. день
" execute "edit" "archive/".strftime("%Y_%m_%d.org")
nmap <localleader>cd gg/\* Выполнен<CR>VGy:execute "edit" "archive/".strftime("%Y_%m_%d.org")<CR>P:w<CR><C-^>jVGd:e<CR>
" Перенести задачи из tasks в week
"if has("nvim")
"    nmap <localleader>tw dar:noautocmd split week.org<CR>Gp<CR>:q<CR>dd
"else
nmap <localleader>tw dar:noautocmd split week.org<CR>Gp:e<CR>:q<CR>
"endif
"<C-^>
"nmap <localleader>tt dar:noautocmd split tasks.org<CR>Gp<C-^>
nmap <localleader>tt dar:noautocmd split tasks.org<CR>Gp:e<CR>:q<CR>
" Перенести задачи из week в today
"if has("nvim")
"    nmap <localleader>td dar:noautocmd split today.org<CR>/\* Выполнен<CR>P:q<CR>dd
"else
    nmap <localleader>td dar:noautocmd split today.org<CR>/\* Выполнен<CR>P:e<CR>:q<CR>
"endif
nmap <localleader>dd dar:noautocmd split to_delete.org<CR>Gp:e<CR>:q<CR>
