" Appearance & interface
filetype plugin indent on      " enable filetype plugin with indent
syntax on                      " enable syntax highlighting

"Use Vim settings, rather then Vi settings (much better!).
"This must be first, because it changes other options as a side effect.
set nocompatible

set number                     " show line numbers
set showcmd                    " show incomplete cmds down the bottom
set showmode                   " show current mode down the bottom
set ruler                      " shows the status line down the bottom
"set gcr=a:blinkon0             " disable cursor blink
set showmatch                  " show closing braces
set list listchars=tab:\ \ ,trail:· " display trailing spaces & tabs
set autowriteall               " write the contents of the file on buffer change
set autoread                   " reload files changed outside vim
set hlsearch                   " highlight all search results

"indent settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" menu
set wildmenu


" Line wrapping
set wrap                       " wrap lines
set linebreak                  " wrap lines at convenient points


" Search
set incsearch                  " incremental search, hit '<CR>' to stop
set hlsearch                   " highlight search results
set ignorecase                 " ignore case while searching
set smartcase                  " override ignorecase while using capitals

let maplocalleader = '\'
let mapleader = ','

" wrap in vimdiff
au VimEnter * if &diff | execute 'windo set wrap' | endif

" Plugins
" export vim-plug list
if filereadable(expand("~/.vim/plug-list.vim"))
  source ~/.vim/plug-list.vim
endif

"if !has("gui_running")
"  let g:solarized_termcolors=16
"endif
"set background=dark
"colorscheme NeoSolarized
if filereadable(expand("~/.vimrc_background"))
  "let base16colorspace=256          " Remove this line if not necessary
  source ~/.vimrc_background
endif

" Persistent Undo
if has('persistent_undo')
  silent !mkdir ~/.vim/backups > /dev/null 2>&1
  set undodir=~/.vim/backups
  set undofile
endif

" Tags
set tags=./tags;
"let g:easytags_async = 1
"let g:easytags_dynamic_files = 2
"let g:easytags_always_enabled = 1
"let g:easytags_opts = ['--regex-ruby="/^[ \t]*scope[ \t]*:([a-zA-Z0-9_]+)/\1/"']
"autocmd FileType ruby let b:easytags_auto_highlight = 0 " very slow highlight ruby files
let g:rails_ctags_arguments = ['--languages=ruby . $(bundle list --paths)', '--regex-ruby="/^[ \t]*scope[ \t]*:([a-zA-Z1-9_]+)/\1/"']

" Ruby

"" Disable turbux globally; let it work only for ruby files
let g:no_turbux_mappings = 1

au Filetype ruby
            \ set shiftwidth=2 |
            \ set softtabstop=2 |
            \ set tabstop=2

autocmd FileType ruby,eruby
                 \ let g:rubycomplete_buffer_loading = 1 |
                 \ let g:rubycomplete_classes_in_global = 1 |
                 \ let g:rubycomplete_rails = 1 |
                 \ let g:rubycomplete_use_bundler = 1

" UltiSnippets
" inoremap <c-x><c-k> <c-x><c-k>
let g:UltiSnipsExpandTrigger = "<s-tab>"
let g:UltiSnipsJumpForwardTrigger = "<s-tab>"
let g:UltiSnipsJumpBackwardTrigger = "<c-tab>"

" Latex
" TIP: if you write your \label's as \label{fig:something}, then if you
" type in \ref{fig: and press you will automatically cycle through
" all the figure labels. Very useful!
set iskeyword+=:

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor = "latex"

" Vim-Latex spell checking : ignore comment regions
let g:tex_comment_nospell=1

let g:vimtex_view_method = 'zathura'
"
" Disable custom warnings based on regexp
let g:vimtex_quickfix_ignore_filters = []
"      \ 'Marginpar on page',
"      \]
let g:vimtex_quickfix_open_on_warning = 0

if has('nvim')
    let g:vimtex_compiler_progname = 'nvr'
endif

au Filetype tex
            \ set spell |
            \ set spelllang=ru,en

let g:vimtex_fold_enabled = 1

" To enable the plugin match-up after installation, add the followingto
let g:matchup_override_vimtex = 1

" from vimtex docs
"call deoplete#custom#var('omni', 'input_patterns', {
"        \ 'tex': g:vimtex#re#deoplete
"        \})

let g:vimtex_grammar_vlty = {'lt_command': 'languagetool'}

" Deoplete (completion)
"call deoplete#custom#option({
"\ 'smart_case': v:true,
"\ })
"let g:deoplete#enable_at_startup = 1
"" to support deoplete-jedi
"let g:jedi#completions_enabled = 0
"let g:deoplete#sources#jedi#show_docstring = 1
"autocmd FileType ruby,json
"       \ call deoplete#custom#buffer_option('auto_complete', v:false)


" let g:lsp_log_verbose = 1
" let g:lsp_log_file = expand('~/vim-lsp.log')

" Navigation
runtime macros/matchit.vim " enables % jumping with intelligence (<body> → </body>, do → end)

let g:EasyMotion_do_mapping = 1 " Disable default mappings
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_use_smartsign_ru = 1

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" Thesaurus Query 

let g:tq_language=['en','ru']


" Enable Neomake to run on builds.
"" {{{ neomake options
"let g:neomake_list_height = 3
"let g:neomake_open_list = 0
"let g:neomake_serialize = 1
"let g:neomake_serialize_abort_on_error = 1
let g:neomake_verbose = 1
"let g:neomake_javascript_enabled_makers = ['eslint']
""let g:neomake_python_enabled_makers = ['python', 'flake8']
"let g:neomake_python_pylint_exe = 'pylint2'
"let g:neomake_python_enable_makers = ['pylint']
"let g:neomake_scss_enabled_makers = ['scss-lint']
"let g:neomake_sh_enabled_makers = ['shellcheck']
"let g:neomake_ruby_enabled_makers = ['rubocop', 'mri']
"let g:neomake_vim_enabled_makers = ['vint']
"let g:neomake_elixir_enabled_makers = ['mix', 'credo']
" }}}
"autocmd BufNewFile,BufRead *.rb let g:neomake_ruby_enabled_makers = ['mri', 'rubocop', 'reek', 'rubylint']
"autocmd BufNewFile,BufRead *_spec.rb,Gemfile,config/routes.rb let g:neomake_ruby_enabled_makers = ['mri', 'rubocop', 'reek']
"autocmd BufNewFile,BufRead *.rb let g:neomake_ruby_enabled_makers = ['mri', 'rubocop', 'reek']

"au BufWritePost * Neomake
"au BufReadPost  * Neomake

if has("multi_byte")
    if &termencoding == ""
        let &termencoding = &encoding
    endif
    set encoding=utf-8
    setglobal fileencoding=utf-8 bomb
    set fileencodings=utf-8,cp1251,cp866,koi8-r,latin1
    set fileformats=unix,mac,dos
endif

let g:languagetool_jar="/usr/share/java/languagetool/languagetool-commandline.jar"


" XkbSwitch
let g:XkbSwitchEnabled = 1

let g:XkbSwitchNLayout = 'us'
let g:XkbSwitchILayout = 'us'

let g:XkbSwitchIMappings = ['ru']
let g:XkbSwitchSkipIMappings = {'*': ["'", '"', '[', '(']}
let g:XkbSwitchIMappingsSkipFt = ['tex', 'md', 'org']

let g:XkbSwitchAssistNKeymap = 1    " for commands r and f
let g:XkbSwitchAssistSKeymap = 1    " for search lines
let g:XkbSwitchDynamicKeymap = 1
let g:XkbSwitchKeymapNames = {'ru' : 'russian-jcukenwin'}

" UTL
let g:utl_cfg_hdl_scm_http_system = "xdg-open %u"
"'%u#%f'"
" Pencil

"augroup pencil
"  autocmd!
"  autocmd FileType markdown call pencil#init({'wrap': 'hard', 'autoformat': 1})
"  autocmd FileType tex     call pencil#init({'wrap': 'soft', 'autoformat': 1, 'conceallevel': 0})
"augroup END

" vim-dasht
let g:dasht_filetype_docsets = {} " filetype => list of docset name regexp

" When in Elixir, also search Erlang:
let g:dasht_filetype_docsets['elixir'] = ['erlang']
let g:dasht_filetype_docsets['cpp'] = ['^c$', 'boost', 'OpenGL']
let g:dasht_filetype_docsets['python'] = ['(num|sci)py', 'pandas']
let g:dasht_filetype_docsets['html'] = ['css', 'js', 'bootstrap']
let g:dasht_filetype_docsets['ruby'] = ['Ruby*']

" Ack.vim
if executable('rg')
  let g:ackprg = 'rg --vimgrep --smart-case -H --column --sortr path '
elseif executable('ag')
  "let g:ackprg = 'ag --vimgrep --smart-case'
  let g:ackprg = 'ag --vimgrep  --smart-case -H --nocolor --nogroup --column'
  " Default: " -s -H --nocolor --nogroup --column"
endif

cnoreabbrev rg Ack
cnoreabbrev ack Ack

" Any empty ack search will search for the work the cursor is on
let g:ack_use_cword_for_empty_search = 1

let g:ackhighlight = 1

" Easy-motion dot support
omap z <Plug>(easymotion-t)
let g:EasyMotion_keys='hklyuiopnm,qwertzxcvbasdgjf;'

" "-" (ASCII 45)
autocmd FileType tex let b:surround_45 = "\\{ \r \\}"


" From vim-stay help
" The following, non-standard 'viewoptions' settings are recommended:
set viewoptions=cursor,folds,slash,unix

" VIM alternative to Semshi
let g:python_highlight_all = 1

" vim-test
let test#custom_runners = {'Sage': ['pytest', 'nosetests']}
let test#enabled_runners = ["sage#pytest", "ruby#rspec", "python#pytest"]

if has("nvim")
    "let test#strategy = 'neovim'
    let test#strategy = 'vimux'
else
    let test#strategy = 'vimux'
    "let test#strategy = 'neovim'
    "let test#strategy = 'dispatch_background'
endif
let g:ultest_use_pty = 1
"
" Pandoc
let g:pandoc#syntax#codeblocks#embeds#langs = ["ruby", "literatehaskell=lhaskell", "bash=sh"]

" Firenvim
autocmd BufEnter mail.yandex.ru_*.txt set filetype=mail
autocmd BufEnter rm.optimalcity.ru_*.txt set filetype=markdown
autocmd BufEnter gitlab.optimalcity.ru_*.txt set filetype=markdown

let g:firenvim_config = {
    \ 'globalSettings': {
        \ 'alt': 'all',
    \ },
    \ 'localSettings': {
        \ 'https?://chat.openai.com/.*': {
            \ 'selector': '',
            \ 'priority': 0
        \ }
    \ }
\ }

let g:vim_ai_chat = {
\  "options": {
\    "model": "gpt-4",
\  },
\}

let g:codeium_enabled = v:false
let g:codeium_filetypes = {
            \ 'ruby': v:true,
            \ 'latex': v:true,
            \ }

source ~/.vim/vimux_shell_last_command.vim
source ~/.vim/mappings.vim
source ~/.vim/ddc.vim

if has('nvim')
    source ~/.vim/init.vim
    source ~/.vim/nvim-lsp.vim
else
    source ~/.vim/vim-lsp.vim
endif
