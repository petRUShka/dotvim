function! VimuxRunLastShellCommand()
  if !exists("g:VimuxRunnerIndex") || _VimuxHasRunner(g:VimuxRunnerIndex) == -1
    call VimuxOpenRunner()
  endif

  let resetSequence = _VimuxOption("g:VimuxResetSequence", "q C-u")
  call VimuxSendKeys("Up Enter")
endfunction
