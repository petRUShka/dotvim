" FZF, file finder
nmap <C-f> :Files<CR>
nmap <C-b> :Buffers<CR>

" Dasht bindings
" search related docsets
nnoremap <silent> <Leader>K :call Dasht([expand('<cword>'), expand('<cWORD>')])<Return>
" search ALL the docsets
nnoremap <silent> <Leader><Leader>K :call Dasht([expand('<cword>'), expand('<cWORD>')], '!')<Return>


" Ruby
"" RSpec.vim mappings
au Filetype ruby
            \ map <leader>t <Plug>SendTestToTmux |
            \ map <leader>T <Plug>SendFocusedTestToTmux

nmap <Leader>vl :call VimuxRunLastShellCommand()<CR>


" vim-test
augroup vimtestbindings
  autocmd! vimtestbindings
  autocmd Filetype sage,*python*,ruby nmap <buffer> <silent> <leader>t :TestNearest<CR>
  autocmd Filetype sage,*python*,ruby nmap <buffer> <silent> <leader>T :TestFile<CR>
  autocmd Filetype sage,*python*,ruby nmap <buffer> <silent> <leader>a :TestSuite<CR>
  autocmd Filetype sage,*python*,ruby nmap <buffer> <silent> <leader>l :TestLast<CR>
  autocmd Filetype sage,*python*,ruby nmap <buffer> <silent> <leader>g :TestVisit<CR>
augroup end
