call plug#begin('~/.vim/plugged')
" Colorscheme
Plug 'junegunn/vim-plug'
Plug 'chriskempson/base16-vim'


Plug 'janko/vim-test'

Plug 'bfrg/vim-jq'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Plug 'bogado/file-line'
" "Plug 'tpope/vim-sleuth' " set identation options based on current identation
" "of file
Plug 'mileszs/ack.vim'
Plug 'bronson/vim-visual-star-search'
" Plug 'ervandew/matchem'
" Plug 'andymass/vim-matchup'
" Plug 'easymotion/vim-easymotion'
" Plug 'ron89/thesaurus_query.vim'
" Plug 'eugen0329/vim-esearch'
" "Plug 'wakatime/vim-wakatime'
" Plug 'junegunn/vim-easy-align'

Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

Plug 'machakann/vim-highlightedyank'
"" " Plug 'AndrewRadev/splitjoin.vim' " One liner to multiliner and vice verca
Plug 'jalvesaq/vimcmdline'
"" Plug 'tpope/vim-dispatch'
"" Plug 'Konfekt/vim-office'
"" Plug 'ActivityWatch/aw-watcher-vim'
"" 
"" Plug 'rickhowe/diffchar.vim'
"" Plug 'AndrewRadev/linediff.vim'
"" 
" Resotre last edit position
Plug 'dietsche/vim-lastplace'
Plug 'vim-autoformat/vim-autoformat'
 
" org
if has("nvim")
    Plug 'nvim-treesitter/nvim-treesitter'
    Plug 'kristijanhusak/orgmode.nvim'
else
    Plug 'jceb/vim-orgmode'
    Plug 'tpope/vim-speeddating'
    Plug 'chrisbra/NrrwRgn'
    Plug 'inkarkat/vim-SyntaxRange'
endif

"Plug 'Exafunction/codeium.vim', { 'branch': 'main' }
Plug 'codota/tabnine-nvim', { 'do': './dl_binaries.sh' }
 
" Snippets
"" Track the engine.
Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
" Plug 'prabirshrestha/async.vim'
" Plug 'thomasfaingnaert/vim-lsp-snippets'
" Plug 'thomasfaingnaert/vim-lsp-ultisnips'
" 
" "Plug 'euclio/gitignore.vim' " adds the entries in a .gitignore file to 'wildignore'
Plug 'tpope/vim-unimpaired'

" Move, Copy, etc
Plug 'tpope/vim-eunuch'

Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
" 
" 
" 
" " Tags support
" Plug 'ludovicchabant/vim-gutentags'
" "Plug 'majutsushi/tagbar'
" 
" Ruby
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-bundler'
" Plug 'tpope/vim-rails'
" Plug 'Shougo/vimproc.vim', {'do' : 'make'}
" Plug 'christoomey/vim-rfactory'
" "" Run Rspec from vim in tmux pane
Plug 'benmills/vimux'
" Plug 'jgdavey/vim-turbux'
" "Plug 'christoomey/vim-tmux-navigator' " Seamless navigation between tmux panes and vim splits 
" Rails
" Plug 'slim-template/vim-slim'
 
 
" Git
Plug 'tpope/vim-fugitive' ", {'commit': 'c0b6fae'} https://github.com/tpope/vim-fugitive/issues/1117
"" "Plug 'mhinz/vim-signify'
if has('nvim')
    Plug 'nvim-lua/plenary.nvim'
    " Forse ft to sage
    Plug 'lewis6991/gitsigns.nvim'
else
    Plug 'airblade/vim-gitgutter'
endif
" Plug 'rhysd/committia.vim'
" 
" Latex
Plug 'lervag/vimtex'
" Plug 'rhysd/vim-grammarous'
" 
" 
"Plug 'neomake/neomake' " ale is alternative
" 
"if has('nvim')
"  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
"  Plug 'Shougo/deoplete.nvim'
"  Plug 'roxma/nvim-yarp'
"  Plug 'roxma/vim-hug-neovim-rpc'
"endif
" "Plug 'zchee/deoplete-jedi'
" Plug 'lighttiger2505/deoplete-vim-lsp'
" 
" Plug 'wellle/tmux-complete.vim'
" " Undo
" Plug 'sjl/gundo.vim'
" 
" Plug 'udalov/kotlin-vim'
" 
" Python
Plug 'davidhalter/jedi-vim' ", {'for': ['python', 'sage', 'sage.python']}
Plug 'Vimjas/vim-python-pep8-indent' ", {'for': ['python', 'sage', 'sage.python']}
Plug 'nathanaelkane/vim-indent-guides' ", {'for': ['python', 'sage', 'sage.python']}
"if has('nvim')
"  Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
"else
Plug 'vim-python/python-syntax' " Better than semshi with sage code
" Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}
" "endif
Plug 'Konfekt/FastFold'
Plug 'tmhedberg/SimpylFold' ", {'for': ['python', 'sage', 'sage.python']}
" " Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'jeetsukumaran/vim-pythonsense'
Plug 'psf/black', { 'branch': 'stable' }
 
" " Ansible
Plug 'pearofducks/ansible-vim', { 'do': 'cd ./UltiSnips; python2 generate.py' }
" 
" Plug 'powerman/vim-plugin-AnsiEsc'
" " Wakkatime
" " Plug 'wakatime/vim-wakatime'
" 
" " Documentation
Plug 'sunaku/vim-dasht'
" 
" " XKB
" Plug 'lyokha/vim-xkbswitch'
" 
" " Writers
" " Plug 'reedes/vim-pencil'
" Plug 'godlygeek/tabular'
" Plug 'preservim/vim-markdown'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

" Plugin 'godlygeek/tabular'
" Plugin 'preservim/vim-markdown'
" Plug 'dbmrq/vim-ditto'
" Plug 'mzlogin/vim-markdown-toc'
" 
" 
" " auto set tab settings
" Plug 'tpope/vim-sleuth'
" Plug 'sgur/vim-editorconfig'
" 
Plug 'qpkorr/vim-bufkill'
" Plug 'zhimsel/vim-stay'
" 
" Plug 'chrisbra/csv.vim'
" 
" Plug 'mtdl9/vim-log-highlighting'
" 
"" Nginx
"Plug 'chr4/nginx.vim'
"Plug 'chr4/sslsecure.vim'

if has('nvim')
    Plug 'williamboman/mason.nvim'
    Plug 'williamboman/mason-lspconfig.nvim'
    Plug 'neovim/nvim-lspconfig'
    Plug 'jose-elias-alvarez/null-ls.nvim'
    Plug 'jay-babu/mason-null-ls.nvim'
else
    Plug 'prabirshrestha/vim-lsp'
    Plug 'mattn/vim-lsp-settings'
endif


Plug 'Shougo/ddc.vim'
Plug 'vim-denops/denops.vim'

if has('nvim')
    Plug 'Shougo/ddc-source-lsp'
else
    Plug 'shun/ddc-source-vim-lsp'
endif

" Install your UIs
Plug 'Shougo/ddc-ui-native'

" Install your sources
Plug 'Shougo/ddc-source-around'

" Install your filters
Plug 'Shougo/ddc-matcher_head'
Plug 'Shougo/ddc-sorter_rank'

if has('nvim')
  Plug 'mfussenegger/nvim-dap'
  Plug 'theHamsta/nvim-dap-virtual-text'
  Plug 'suketa/nvim-dap-ruby'
  "Plug 'rcarriga/nvim-dap-ui'
else
  Plug 'dradtke/vim-dap'
endif

Plug 'vim-scripts/utl.vim'
 
Plug 'chrisbra/NrrwRgn'

Plug 'dstein64/vim-startuptime'
Plug 'romainl/vim-cool'

" PBX
Plug 'ipoddubny/asterisk-vim'

Plug 'zeekay/vim-beautify'
"Plug 'rhysd/clever-f.vim'

Plug 'zainin/vim-mikrotik'

" Add plugins to &runtimepath
"
Plug 'tridactyl/vim-tridactyl'
Plug 'kovetskiy/sxhkd-vim'

" Mathematics
Plug 'petRUShka/vim-gap'
Plug 'petRUShka/vim-sage'
Plug 'petRUShka/vim-magma'

" OpenCL
Plug 'petRUShka/vim-opencl'
Plug 'petRUShka/vim-pyopencl'

" CUDA
Plug 'petRUShka/vim-pycuda'


Plug 'madox2/vim-ai'

call plug#end()
