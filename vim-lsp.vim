" vim-lsp
" set foldmethod=expr
" \ foldexpr=lsp#ui#vim#folding#foldexpr()
" \ foldtext=lsp#ui#vim#folding#foldtext()
if executable('ansible-language-server')
    " pip install python-lsp-server
    au User lsp_setup call lsp#register_server({
        \ 'name': 'ansible-language-server',
        \ 'cmd': {server_info->['ansible-language-server']},
        \ 'allowlist': ['ansible'],
        \ })
endif

func! s:lsp_mappings()
	" Prefer native help with vim files
	if &filetype != 'vim'
		nmap <silent><buffer> K  <Plug>(lsp-hover)
	endif

	nmap <silent><buffer> gr     <Plug>(lsp-references)
	nmap <silent><buffer> gi     <Plug>(lsp-peek-implementation)
	nmap <silent><buffer> gy     <Plug>(lsp-peek-type-definition)
	nmap <silent><buffer> <C-]>  <Plug>(lsp-definition)
	nmap <silent><buffer> g<C-]> <Plug>(lsp-peek-definition)
	nmap <silent><buffer> gd     <Plug>(lsp-peek-declaration)
	nmap <silent><buffer> gY     <Plug>(lsp-type-hierarchy)
	nmap <silent><buffer> gA     <Plug>(lsp-code-action)
	nmap <silent><buffer> ,s     <Plug>(lsp-signature-help)
	nmap <silent><buffer> [d     <Plug>(lsp-previous-diagnostic)
	nmap <silent><buffer> ]d     <Plug>(lsp-next-diagnostic)
	nmap <buffer> <Leader>rn     <Plug>(lsp-rename)
	nmap <buffer> <Leader>F      <plug>(lsp-document-format)
	vmap <buffer> <Leader>F      <plug>(lsp-document-range-format)
endfunc

augroup LSP | au!
    autocmd FileType ansible,python,ruby,go,perl,json,yaml,vim,javascript,sh,lua,tex,rust,c,cpp setlocal omnifunc=lsp#complete
                \ | call s:lsp_mappings()
                \ | setlocal foldmethod=expr
                      \ foldexpr=lsp#ui#vim#folding#foldexpr()
                      \ foldtext=lsp#ui#vim#folding#foldtext()
augroup END
