﻿" You must set the default ui.
" Note: native ui
" https://github.com/Shougo/ddc-ui-native
call ddc#custom#patch_global('ui', 'native')

" Use around source.
" https://github.com/Shougo/ddc-source-around
call ddc#custom#patch_global('sources', ['around'])

" Use matcher_head and sorter_rank.
" https://github.com/Shougo/ddc-matcher_head
" https://github.com/Shougo/ddc-sorter_rank
call ddc#custom#patch_global('sourceOptions', #{
      \ _: #{
      \   matchers: ['matcher_head'],
      \   sorters: ['sorter_rank']},
      \ })

" Change source options
call ddc#custom#patch_global('sourceOptions', #{
      \   around: #{ mark: 'A' },
      \ })
call ddc#custom#patch_global('sourceParams', #{
      \   around: #{ maxSize: 500 },
      \ })

" Customize settings on a filetype
" call ddc#custom#patch_filetype(['c', 'cpp'], 'sources',
"       \ ['around', 'clangd'])
" call ddc#custom#patch_filetype(['c', 'cpp'], 'sourceOptions', #{
"       \   clangd: #{ mark: 'C' },
"       \ })
" call ddc#custom#patch_filetype('markdown', 'sourceParams', {
"       \   around: #{ maxSize: 100 },
"       \ })

if has("nvim")
    call ddc#custom#patch_global('sources', ['lsp'])
    call ddc#custom#patch_global('sourceOptions', #{
                \   nvim-lsp: #{
                \     mark: 'lsp',
                \     forceCompletionPattern: '\.\w*|:\w*|->\w*' },
                \ })

    " Use Customized labels
    "call ddc#custom#patch_global('sourceParams', #{
    "            \   nvim-lsp: #{ kindLabels: #{ Class: 'c' } },
    "            \ })
else
    " https://github.com/shun/ddc-source-vim-lsp
    call ddc#custom#patch_global('sources', ['vim-lsp'])
    call ddc#custom#patch_global('sourceOptions', {
                \ 'vim-lsp': {
                \   'matchers': ['matcher_head'],
                \   'mark': 'lsp',
                \ },
                \ })

    " if you want to use the unsupported CompleteProvider Server,
    " set true by'ignoreCompleteProvider'.
    " call ddc#custom#patch_filetype(['css'], {
    "    \ 'sourceParams': {
    "    \   'vim-lsp': {
    "    \     'ignoreCompleteProvider': v:true,
    "    \   },
    "    \ },
    "    \ })
endif

" Mappings

" <TAB>: completion.
inoremap <silent><expr> <TAB>
\ pumvisible() ? '<C-n>' :
\ (col('.') <= 1 <Bar><Bar> getline('.')[col('.') - 2] =~# '\s') ?
\ '<TAB>' : ddc#map#manual_complete()

" <S-TAB>: completion back.
inoremap <expr><S-TAB>  pumvisible() ? '<C-p>' : '<C-h>'

" Use ddc.
call ddc#enable()

